	#Used the following link to create the functions.
#https://machinelearningmastery.com/autoregression-models-time-series-forecasting-python/

from pandas.plotting import autocorrelation_plot

from pandas import DataFrame
from statsmodels.tsa.arima_model import ARIMA
from statsmodels.tsa.ar_model import AR
from statsmodels.graphics import tsaplots

from sklearn.metrics import mean_squared_error

from matplotlib import pyplot as plt
from pandas.plotting import lag_plot
import pandas as pd
import numpy as np
import tools

class ARIMA_model:
	def __init__(self, data = None, train = None, test = None):
		self.data = data

	def set_data(self, data):
		self.data = data

	def set_train_test_data(self, train, test):
		self.train = train
		self.test = test

	def correlation_graphs(self):
		"""
        correlation_graps:
        - plots: Auto-Correlation Grapgh, Partial Auto-Correlation Graph, 
        	Correlation graph for lag of 1.
        """

		#coverting numpy array to series might be inefficient that could be fixed
		series = pd.Series(self.data)
		#plots the lag plot between y and y+1
		lag_plot(series)
		plt.show()
		
		#numerical proof of correlation between t and t+1
		df = DataFrame(self.data)
		df = pd.concat([df, df.shift(1)], axis = 1)
		df.columns = ['t', 't+1']
		result = df.corr()
		print(result)
		
		#calculates the correlation ebtween time t and i-lag for every lag.
		autocorrelation_plot(series)
		plt.show()

		fig = tsaplots.plot_pacf(self.data, lags=1000)
		plt.show()


	def diff_data(self):
		"""
		Data is differentiated up to f'''(x) and displayed
		"""
		data_diff1 = tools.Other.diff(self.data)
		data_diff2 = tools.Other.diff(data_diff1)
		data_diff3 = tools.Other.diff(data_diff2)

		fig, axs = plt.subplots(nrows=4, ncols=1, sharex=True, sharey=True, figsize=(16,8))
		#fig.text(0.5, 0.04, 'common X', ha='center')
		#fig.text(0.04, 0.5, 'common Y', va='center', rotation='vertical')

		axs[0].plot(self.data)
		axs[0].set_title("Data")
		axs[1].plot(data_diff1)
		axs[1].set_title("First Derivative")
		axs[2].plot(data_diff2)
		axs[2].set_title("Second Derivative")
		axs[3].plot(data_diff3)
		axs[3].set_title("Third Derivative")

		#an additional subplot that covers the figure, for common labels
		fig.add_subplot(111, frameon=False)
		plt.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
		plt.grid(False)
		plt.xlabel("Day", fontsize=16)
		plt.ylabel("Price", fontsize=16)

		fig.tight_layout()

		plt.show()


	def predict_using_autoregression(self):
		"""
		use AR method from statsmodel to predict future prices.
		It determins number of terms automatically
		- Good to fit data to this model and get number of terms before ARIMA.
		"""
		train, test = self.data[:1750], self.data[1750:2000]
		train_diff = tools.Other.diff(train)

		model = AR(train_diff)
		model_fit = model.fit()
		print('Lag: %s' % model_fit.k_ar)
		print('Coefficients: %s' % model_fit.params)

		#predict the next 500 days
		predictions = model_fit.predict(start=len(train), end=len(train)+len(test)-1, dynamic=False)
		result = tools.Other.integrate(predictions, test[0]) 

		for i in range(len(result)):
			print('predicted=%f, expected=%f' % (result[i], test[i]))

		error = mean_squared_error(test, result)
		print('Test MSE: %.3f' % error)
		plt.plot(test)
		plt.plot(result, color='red')
		plt.show()



	def predict_using_ARIMA(self):
		"""
		Predict using ARIMA.
		"""
		train, test = self.data[:1750], self.data[1750:2000]
		    
		#
		model = ARIMA(train, order=(10, 1, 5))
		model_fit = model.fit(disp=0)
		print(model_fit.summary())

		print("start predictions")
		result = model_fit.predict(start=len(train), end=len(train)+len(test)-1, dynamic=False)
		#result = tools.Other.integrate(predictions, test[0]) 

		for i in range(len(result)):
			print('predicted=%f, expected=%f' % (result[i], test[i]))

		error = mean_squared_error(test, result)
		error = tools.Error.rmse(test,result)
		print('Test rMSE: %.5f' % error)
		plt.plot(test)
		plt.plot(result, color='red')
		plt.show()


	def my_ARIMA(self):
		train, test = self.data[:1500], self.data[1500:2000]
		train_diff = tools.Other.diff(train)

		model = AR(train_diff)
		model_fit = model.fit()
		print('Lag: %s' % model_fit.k_ar)
		print('Coefficients: %s' % model_fit.params)

		#predict the next 500 days
		predictions = model_fit.predict(start=len(train), end=len(train)+len(test)-1, dynamic=False)
		AR_result = tools.Other.integrate(predictions, test[0])

		#TODO - should be over progressive prediction, not TEST array
		len_MA = 20
		temp = np.concatenate([train[-(len_MA-1):], test])
		MA_result = self.running_mean(temp, len_MA)

		final_result = AR_result - MA_result
		plt.plot(test)
		plt.plot(AR_result, color='red')
		plt.plot(MA_result, color="green")
		plt.show()

		error = tools.Error.rmse(test,final_result)
		print('Test rMSE: %.5f' % error)



	def running_mean(self, x, N):
		cumsum = np.cumsum(np.insert(x, 0, 0)) 
		return (cumsum[N:] - cumsum[:-N]) / float(N)

	def dickey_fuller_test():
	    #https://towardsdatascience.com/machine-learning-part-19-time-series-and-autoregressive-integrated-moving-average-model-arima-c1005347b0d7
	    pass
