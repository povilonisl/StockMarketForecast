import sys

import numpy as np
import seaborn as sns
from matplotlib import pyplot as plt

import sql_db
import pre_process
import tools
import ESN_stock
from statistical_method import ARIMA_model
from tools import Output

import LSTM_stock


#---------------- Functions ----------------
def get_esn_hyper_param():
	temp = esn.opt_hyperparamaters(15, 10, (250, 750), (0.8, 1.4), 
		(0.1, 0.5), (0.0001, 0.001))
	print(temp.x[0])
	print(temp.x[1])
	print(temp.x[2])
	print(temp.x[3])
	print(temp.fun)

	return temp.x








#--------------------------------------------------------------
#store the data location
data_location = '../'
if len(sys.argv) > 1:
    data_location = sys.argv[1]

#Pre-process the data
pre = pre_process.Pre_process(data_location)
#the data split up into train and test
array_x_train, array_y_train, array_x_test, array_y_test, array_all_mid_data = (
	pre.pre_process_data(window_size=4, time_steps_ahead=0, split_data_on=0.85))

print(f"shape of array_x_train: {array_x_train}")


x_train, y_train = array_x_train[0][:3200], array_y_train[0][:3200]
x_test, y_test = array_x_test[0][:160], array_y_test[0][:160]


#basic hyper-params for the data
"""
train_len = 1500
future = 100
future_total = 500
"""
future = 1
future_total = 160


#-------- Predict Future Price-----------------
#Forecast price using ESN

esn = ESN_stock.my_ESN()
esn.set_data_param(x_train, y_train, x_test, y_test, future, future_total, 1, 1)

get_esn_param = False
if get_esn_param:
	hyper_paramanters = get_esn_hyper_param()
else:
	#rmse: 0.07296481243331329
	hyper_paramanters = [679, 1.093878594251079, 0.3354800055937188, 0.0008682471095634399]

esn_yhat = esn.predict_ESN(23, *hyper_paramanters)
print(esn_yhat)



#Use ARIMA/MA/AR to predict
"""
arima = ARIMA_model(array_x_train[0][0][0:2000])
arima.set_data(array_x_train[0][0][0:2000])

arima.my_ARIMA()
"""













#-------------------------------------- TEST ---------------------------------
#------------ TEST SQL ----------------
"""
my_sql_db = sql_db.SQL_DB()
my_sql_db.set_sql_db_path(data_location)
#my_sql_db.create_table()
my_sql_db.something()
"""



#-------- TEST LSTM ------------------------

"""
lstm = LSTM_stock.my_LSTM()
lstm_y_hat = lstm.lstm_stateful(x_train, y_train, x_test, y_test)
"""

#lstm.lstm(x, y, array_x_train[0][500:600], array_y_train[0][500:600])
#lstm.lstm(x, y, x_test, y_test)

"""
print(type(array_x_train))
print(len(array_x_train))
print(len(array_x_train[0]))
print(len(array_x_train[0][0]))
print(array_x_train[0][0][0])
print(len(array_y_train[0]))
print(array_y_train[0][0])
"""



#------------------Test tools.Output -------------------------------


x_data = range(len(y_test))
out = Output(1)
out.plot_to_new_axs([x_data, x_data], [y_test, 
	esn_yhat.reshape(esn_yhat.shape[0]*esn_yhat.shape[1])], ['y_test', 'y_hat'])
out.display()















"""
Proper way to comment:


    Compute spectral radius of a square 2-D tensor for stacked-ESN
    :param m: squared 2D tensor
    :param leaky_rate: Layer's leaky rate
    :return:
"""





#------------------------------------ NOTES ----------------------------------
"""
Maybe I should use adjusted closing price, instead of average of low and high.
"""