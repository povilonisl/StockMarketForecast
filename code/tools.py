"""
Author: Lukas Povilonis

Tools for ML
"""

import numpy as np

from matplotlib import pyplot as plt



class Error:

    @staticmethod
    def rmse(yhat, yreal):
        """
        The distance on average of the predicted point from the expected point.

        yhat: the predicted y
        yreal: the real/expected y
        return rmse
        """
        return np.sqrt(np.mean((yhat - yreal)**2))

    @staticmethod
    def mse(yhat, yreal):
        """
        Mean squared error.

        yhat: the predicted y
        yreal: the real/expected y
        return rmse
        """
        return np.mean((yhat - yreal)**2)

class Other:
    @staticmethod
    def diff(data, interval=1):
        temp = np.zeros(len(data))
        for i in range(interval, len(data)):
            temp[i] = data[i] - data[i-interval]
        return temp

    @staticmethod
    def integrate(diff_data, first_data_point = 0, interval=1):
        temp = np.zeros(len(diff_data))
        temp[0] = first_data_point
        for i in range(interval, len(diff_data)):
            temp[i] = diff_data[i] + temp[i-1]
        return temp
        


    
class Output:
    """
    Allows to plot multiple list of data points on the same or different 
    axs on the same GUI.
    """

    def __init__(self, no_of_axs):
        """
        Initialise and set no_of_axs to use
        :param no_of_axs: Total number of axs will be used.
        """
        self.no_of_axs = no_of_axs
        self.fig, self.axs = plt.subplots(nrows = self.no_of_axs, figsize=(16,8))
        self.axs_no = -1
        self.avb_colours = ['black', 'green', 'yellow', 'blue', 'red', 'cyan', 'magenta', 'brown', 'purpule'] 


    def plot_to_new_axs(self, x_data, y_data, lbl):
        """
        Increment axs_no, hence use new axs to plot data points.
        :param x_data: [[x_data points]] -> list of list of int
        :param y_data: [[y_data points]] -> list of list of float
        :param lbl: [Labels] ->  list of String, label will be used in Legend.
        """
        self.axs_no += 1
        self.plot_to_axs(x_data, y_data, lbl, self.axs_no)


    def plot_to_prev_axs(self, x_data, y_data, lbl):
        """
        axs_no is not changed, uses the same axs as previous plot
        :param x_data: [[x_data points]] -> list of list of int
        :param y_data: [[y_data points]] -> list of list of float
        :param lbl: [Labels] ->  list of String, label will be used in Legend.
        """
        self.plot_to_axs(x_data, y_data, lbl, self.axs_no)


    def plot_to_axs(self, x_data, y_data, lbl, axs_to_use):
        """
        Plots the data points to specific specified axs by axs_to_use
        :param x_data: [[x_data points]] -> list of list of int
        :param y_data: [[y_data points]] -> list of list of float
        :param lbl: [Labels] ->  list of String, label will be used in Legend.
        :param axs_to_use: int, The axs to use to plot the data points.
        """
        if axs_to_use  >= self.no_of_axs:
            raise ValueError(
                f'Exceeded the number of plots to be displayed: {axs_to_use}/{self.no_of_axs}')

        #check how many axs are used, if only 1, then it's accessed differently.
        if self.no_of_axs == 1:
            #Plot multiple graphs on the same axs
            for x, y, l, c in zip(x_data, y_data, lbl, self.avb_colours[:len(lbl)]):
                self.axs.plot(x, y, color = c, label = l)
        else:
            #Plot multiple graphs on the same axs
            for x, y, l, c in zip(x_data, y_data, lbl, self.avb_colours[:len(lbl)]):
                self.axs[axs_to_use].plot(x, y, color = c, label = l)


    def display(self):
        """
        Display the GUI for axs
        """
        plt.legend()
        plt.show()
