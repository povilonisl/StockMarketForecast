"""
Author: Lukas Povilonis


"""


import glob
import os
import pandas as pd
import numpy as np

from sklearn.preprocessing import MinMaxScaler


class Pre_process:
	def __init__(self, file_path = None):
		self.data_location = file_path


	def set_data_path(self, file_path):
		self.data_location = file_path


	def load_data(self):
		df = pd.read_csv(self.data_location + "temp.csv", delimiter=',', usecols=['Date','smth','Temp'])
		tempreture = df.loc[:,'Temp'].values

		return np.array(tempreture)


	def tanh_estimator(self,mean, std, data):
		"""
		performs tanh estimator on given data, mean and stand diviation.

		:return: normalised data using tanh estimator, data keeps same structure
		"""
		return 0.5*(np.tanh((0.1*(data - mean))/std) + 1)


	def inverse_tanh_estimator(self, mean, std, data):
		"""
		reverses the data back to original scale from tanh-estimator scale.
		There's an error of e-16 to e-17
		:return: data reversered back from tanh-estimator 
		"""
		return np.arctanh(2*data - 1)*10*std + mean


	def pre_process_data(self, window_size = 5, time_steps_ahead = 0, split_data_on = 0.85):
		"""
		pre-process data:
		- using average of opening and closing price
		- tanh estimator to normalise

		:param window_size: how many days of the past to use to predict
		:param time_steps_ahead: how many days in the future to predict, 0 = next entry
		:param split_data_on: how much of data to use for training(90% by def) and testing
		:return: list_x_train: list of list (file) of numpy.ndarray (training input)
		:return: list_y_train: list of list (file) of numpy.ndarray (true output for test)
		:return: list_x_test: list of list (file) of numpy.ndarray (testing input)
		:return: list_y_test: list of list (file) of numpy.ndarray (true output for test)
		:return: list_all_mid_data: list of list (file) of all data points
		"""

		print("Start Pre-processing data")
		list_x_train = [] 
		list_y_train = []
		list_x_test = []
		list_y_test = []
		list_all_mid_data = []

		min_max_scaler = MinMaxScaler()


		files = glob.glob(os.path.join(self.data_location, 'data','*.txt'))
		no_of_files = len(files)
		no_of_features = 1
		no_of_outputs = 1

		for file in range(no_of_files):
			cols = ['Date','Open','High','Low','Close']
			df = pd.read_csv(files[file], delimiter=',',usecols=cols, infer_datetime_format = True)
			df = df.sort_values('Date')

			print('Loaded data from the Kaggle repository')

			df = self.fill_the_blanks(df, cols)

			#Splitting data into training and test, while only taking the average price
			high_prices = df.loc[:,'High'].values
			low_prices = df.loc[:,'Low'].values
			mid_prices = (high_prices+low_prices)/2.0
			mid_prices = mid_prices.reshape(mid_prices.size, 1)
			#----------------TEMPORARY-------------------------------
			mid_prices = mid_prices[:4000]
			#-------------------------------------------------------

			split = int(len(mid_prices)*split_data_on)
			print("length: {}, split_on: {}".format( len(mid_prices), split_data_on))
			train_data = mid_prices[:split]
			test_data = mid_prices[split:]
			print(f" test_data.shape: {test_data.shape}")
			print(f"split: {split}")
			#normalise - tanh estimator
			mean = np.mean(train_data)
			std = np.std(train_data)

			#train_data = self.tanh_estimator(mean, std, train_data)
			#test_data = self.tanh_estimator(mean, std, test_data)


			"""
			train_data = train_data.reshape(-1, 1)
			#print(f"train_data shape: {np.rank(train_data)}")
			#print(f"train_data shape: {train_data.shape}")
			#print(f"train_data: {train_data}")

			train_data = min_max_scaler.fit_transform(train_data)

			test_data = test_data.reshape(-1, 1)
			test_data = min_max_scaler.transform(test_data)
			"""


			#can be used for visualisation and test purposes
			all_mid_data = np.concatenate([train_data, test_data], axis=0)
			list_all_mid_data.append(all_mid_data)


			#initalise the arrays for train and test
			no_of_train_samples = train_data.size - window_size - time_steps_ahead
			no_of_test_samples = test_data.size - window_size - time_steps_ahead

			array_x_train = np.empty([no_of_files, no_of_train_samples, window_size, no_of_features])
			array_y_train = np.empty([no_of_files, no_of_train_samples, no_of_outputs])
			array_x_test = np.empty([no_of_files, no_of_test_samples, window_size, no_of_features])
			array_y_test = np.empty([no_of_files, no_of_test_samples, no_of_outputs])
			#array_y_train = np.empty([no_of_files, no_of_train_samples, time_steps_ahead+1, no_of_outputs])


			#restructure the data into rolling series
			#It starts with first predictable element in train_data array.
			for i in range(window_size + time_steps_ahead, train_data.size):
				#split-up the data into 2 lists one for inputs and another for outputs
				j = i - window_size - time_steps_ahead
				array_x_train[file, j] = train_data[i-window_size-time_steps_ahead : i-time_steps_ahead]
				array_y_train[file, j] = train_data[i-time_steps_ahead:i+1]

			#restructure the data into rolling series
			for i in range(window_size + time_steps_ahead, test_data.size):
				j = i - window_size - time_steps_ahead
				array_x_test[file, j] = test_data[i-window_size-time_steps_ahead : i-time_steps_ahead]
				array_y_test[file, j] = test_data[i-time_steps_ahead:i+1]


		return array_x_train, array_y_train, array_x_test, array_y_test, all_mid_data



	def fill_the_blanks(self, df, cols):
		"""
		Fill in missing rows in df based on the date.
		Only works when other columns (not date) are numeric

    	:param df: The original dateframe with missing rows.
    	:param cols: A list of columns of the dateframe.
    	:return: Sorted dataframe with missing rows added in.
		"""

		#set date to pd.DateTime so it's easier to deal with dates
		df['Date'] = pd.to_datetime(df['Date'])		

		missing_rows = []
		previous_row = None
		for row in df.iterrows():
			if previous_row is not None:
				#if only one day of difference then go to next row.
				if (row[1][0] - previous_row[1][0]).days < 2:
					previous_row = row
					continue

				#calculate the fill and store in temp_list
				temp_list = [0]*(len(previous_row[1]))
				for col in range(1, len(previous_row[1])):
					temp_list[col] = ((row[1][col] - previous_row[1][col])/(row[1][0]- previous_row[1][0]).days) 

				#append a row to missing_rows for each missing day
				for i in range(1, (row[1][0] - previous_row[1][0]).days):
					#calculate the value of each col.
					new_row = [previous_row[1][col] + temp_list[col]*i 
						if col>0 else 0 for col in range(0, len(previous_row[1]))]
					new_row[0] = previous_row[1][0] + pd.Timedelta(days=i)
					missing_rows.append(new_row)

				previous_row = row
			else:
				previous_row = row

		#convert missing_rows to df, append it and sort it
		df2 = pd.DataFrame(missing_rows, columns = cols)
		df = df.append(df2)
		df = df.sort_values('Date')

		return df




"""
----------TEST--------------
N/A
----------------------------
"""




"""
#use it for normalisation later.
from sklearn.preprocessing import MinMaxScaler
sc = MinMaxScaler(feature_range = (0, 1))
training_set_scaled = sc.fit_transform(training_set)



"""