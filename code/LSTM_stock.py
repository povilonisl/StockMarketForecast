import numpy as np
from matplotlib import pyplot as plt
import pandas as pd



from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from keras import optimizers


from sklearn.preprocessing import MinMaxScaler



class my_LSTM:
	def __init__(self):
		pass


	def lstm_stateful(self, x, y, x_test, y_test):
		print(f"x_train.shape: {x.shape}")
		print(f"y_train.shape: {y.shape}")
		print(f"x_test.shape: {x_test.shape}")


		regressor = Sequential()

		regressor.add(LSTM(units = 20, stateful = True, return_sequences = False, batch_input_shape = (32, x.shape[1], 1)))
		regressor.add(Dropout(0.5))
		"""
		regressor.add(LSTM(units = 20, stateful = True, return_sequences = False))
		regressor.add(Dropout(0.2))
		
		regressor.add(LSTM(units = 50, stateful = True, return_sequences = True))
		regressor.add(Dropout(0.2))
		"""

#		regressor.add(Dense(units = 20, activation='relu'))
		regressor.add(Dense(units = 1, activation='sigmoid'))

		optimizer = optimizers.RMSprop(lr=0.00010000)
		regressor.compile(optimizer = optimizer, loss = 'mse')
		#regressor.compile(optimizer = 'adam', loss = 'mse')


		
		print("LSTM: Training")
		
		epochs = 25
		for i in range(epochs):
			print(f"Epoch: {i}/{epochs}")
			regressor.fit(x, y, batch_size = 32, verbose = 1)
			regressor.reset_states()

		"""
		print("LSTM: Predicting")
		predictions = []
		for i in range(len(x_test)):
			x_t = np.array([x_test[i]])
			y_hat = regressor.predict(x_t, batch_size=1)
			#y_hat = regressor.predict_on_batch(x_t)
			predictions.append(y_hat[0][0])
		"""
		predictions = regressor.predict(x_test, batch_size=32)
		

		print(f"y_hat: {predictions[:20]}")
		print(f"y_test: {y_test[:20]}")
		print(f"y_train: {y[:20]}")
		print(f"predictions.shape: {predictions.shape}")

		#return predictions
	

		fig, axs = plt.subplots(nrows=2, figsize=(16,8))
		axs[0].plot(np.concatenate((y.reshape(y.shape[0], ), 
			y_test.reshape(y_test.shape[0],))), color = 'black', label = 'TATA Stock Price')
		axs[0].plot(range(len(y), len(y) + len(y_test)) , predictions, color = 'green', label = 'Predicted TATA Stock Price')
	
		axs[1].plot( y_test, color = 'black', label = 'TATA Stock Price')
		axs[1].plot(predictions, color = 'green', label = 'Predicted TATA Stock Price')

		plt.show()

		return predictions




	def lstm(self, x, y, x_test, y_test):
		x = np.array(x)
		y = np.array(y)
		x_test = np.array(x_test)
		y_test = np.array(y_test)



		regressor = Sequential()

		regressor.add(LSTM(units = 50, return_sequences = True, input_shape = (x.shape[1], 1)))
		regressor.add(Dropout(0.2))

		"""
		regressor.add(LSTM(units = 50, return_sequences = True))
		regressor.add(Dropout(0.2))
		"""
		regressor.add(LSTM(units = 20))
		regressor.add(Dropout(0.2))
		
	

		regressor.add(Dense(units = 1))

		regressor.compile(optimizer = 'adam', loss = 'mean_squared_error')

		regressor.fit(x, y, epochs = 5, batch_size = 32)




		predicted_stock_price = regressor.predict(x_test)
		predicted_stock_price = predicted_stock_price.reshape(predicted_stock_price.shape[0], )
		y_test = y_test.reshape(y_test.shape[0], )
		print(f"y_hat: {predicted_stock_price[:20]}")
		print(f"y_test: {y_test[:20]}")
		print(f"y_train: {y[:20]}")



		#return predicted_stock_price

		#plt.figure(figsize=(16,8))
		fig, axs = plt.subplots(nrows=2, figsize=(16,8))
		axs[0].plot(np.concatenate((y.reshape(y.shape[0], ), y_test)), color = 'black', label = 'TATA Stock Price')
		axs[0].plot(range(len(y), len(y) + len(y_test)) , predicted_stock_price, color = 'green', label = 'Predicted TATA Stock Price')
	
		axs[1].plot( y_test, color = 'black', label = 'TATA Stock Price')
		axs[1].plot(predicted_stock_price, color = 'green', label = 'Predicted TATA Stock Price')

		plt.show()




"""
use: stateful = True

agile

gif flow
git flow - workflow
spring, hyperbonate, elastic search 


AWS
"""