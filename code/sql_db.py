"""
Followed the following tutorial:
https://stackabuse.com/a-sqlite-tutorial-with-python/

Right now it has not real use for StockMarketForecast, just a reference for later use.

"""


import os
import sqlite3


class SQL_DB:
	def __init__(self):
		pass

	def set_data_path(self, path):
		self.path = path

	def set_sql_db_path(self, sql_db_path):
		self.sql_db_path = os.path.join(sql_db_path, 'db/', 'database.sqlite3')

	def db_connect(self):
		con = sqlite3.connect(self.sql_db_path)
		return con

	def create_table(self):
		con = self.db_connect()
		cur = con.cursor()

		'''
		temp_one_sql = """
		CREATE TABLE customers (
			id integer PRIMARY KEY,
			first_name text NOT NULL,
			last_name text NOT NULL)"""

		cur.execute(temp_one_sql)
		
		
		temp_two_sql = """
		CREATE TABLE poducts (
			id integer PRIMARY KEY, 
			name text NOT NULL,
			price real NOT NULL)"""
		cur.execute(temp_two_sql)
		

		temp_three_sql = """
		CREATE TABLE orders (
			id integer PRIMARY KEY,
			date text NOT NULL,
			customer_id integer,
			FOREIGN KEY (customer_id) REFERENCES customers (id))"""
		cur.execute(temp_three_sql)

		temp_four_sql = """
		CREATE TABLE lineitems (
		id integer PRIMARY KEY,
		quantity integer NOT NULL,
		total real NOT NULL, 
		product_id integer,
		order_id integer,
		FOREIGN KEY (product_id) REFERENCES products (id)
		FOREIGN KEY (order_id) REFERENCES orders (id))"""
		cur.execute(temp_four_sql)
		

		product_sql = "INSERT INTO poducts (name, price) VALUES (?, ?)"
		cur.execute(product_sql, ('Introduction to Combinatorics', 7.99))
		cur.execute(product_sql, ('A Guide to Writing Short Stories', 17.99))
		cur.execute(product_sql, ('Data Structures and Algorithms', 11.99))
		cur.execute(product_sql, ('Advanced Set Theory', 16.99))

		'''



	def insert_customer(self, first_name, last_name):
		"""
		insert a row into customer.

		:return: The id of inserted item.
		"""
		sql = """
			INSERT INTO customers (first_name, last_name)
			VALUES (?, ?)"""
		cur = con.cursor()
		cur.execute(sql, (first_name, last_name))
		return cur.lastrowid


	def update(self):
		update_sql = "UPDATE podcuts SET price = ? WHERE id = ?"
		cur.execute(update_sql, (10.99, 2))


	def all_or_nothing(self, con):
		try:
			self.insert_customer()
			#other things to do
			
			#commit the statement
			con.commit()
		except:
			#rollback all database actions since last commit
			con.rollback()
			raise RuntimeError("Uh oh, an error occured...")



	def something(self):
		con = self.db_connect()
		cur = con.cursor()

		#Shows the overall view of DB
		cur.execute("SELECT name FROM sqlite_master WHERE type='table'")
		print(cur.fetchall())

		cur.execute("SELECT sql FROM sqlite_master WHERE type='table' AND name='customers'")
		print(cur.fetchone()[0])

		
		product_sql = "INSERT INTO poducts (name, price) VALUES (?, ?)"
		cur.execute(product_sql, ('Introduction to Combinatorics', 7.99))
		cur.execute(product_sql, ('A Guide to Writing Short Stories', 17.99))
		cur.execute(product_sql, ('Data Structures and Algorithms', 11.99))
		cur.execute(product_sql, ('Advanced Set Theory', 16.99))


		cur.execute("SELECT id, name, price FROM poducts")
		formatted_results = [f"{id:<5}{name:<35}{price:>5}" for id, name, price in cur.fetchall()]
		id, product, price = "Id", "Product", "Price"
		print('\n'.join([f"{id:<5}{product:<35}{price:>5}"] + formatted_results))

		customer_sql = "INSERT INTO customers (first_name, last_name) VALUES (?, ?)"
		cur.execute(customer_sql, ('Alan', 'Turing'))
		customer_id = cur.lastrowid
		print(customer_id)

		order_sql = "INSERT INTO orders (date, customer_id) VALUES (?, ?)"
		date = "1944-02-22"
		cur.execute(order_sql, (date, customer_id))
		order_id = cur.lastrowid

		li_sql = """
		INSERT INTO lineitems (order_id, product_id, quantity, total) VALUES (?, ?, ?, ?)"""

		product_id = 1
		cur.execute(li_sql, (order_id, 1, 1, 7, 99))

"""
Study:
- index, sort,  
"""