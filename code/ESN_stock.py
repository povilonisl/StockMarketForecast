"""
Original Author: Matthew Stewart
Original code from:
https://towardsdatascience.com/predicting-stock-prices-with-echo-state-networks-f910809d23d4
https://github.com/mrdragonbear/EchoStateNetworks

"""

import sys

import numpy as np
from matplotlib import pyplot as plt
from pyESN import ESN

import tools
import pre_process

from skopt.space import Real, Integer
from skopt import gp_minimize
from skopt.plots import plot_convergence

class my_ESN:

	def __init__(self):
		pass


	def set_data_param(self, x_train, y_train, x_test, y_test,
	 future, future_total, n_inputs, n_outputs):

		self.x_train = x_train
		self.y_train = y_train
		self.x_test = x_test
		self.y_test = y_test
		self.future = future
		self.future_total = future_total
		self.n_inputs = n_inputs
		self.n_outputs = n_outputs


	def predict_ESN(self, esn_random_state, esn_n_reservoir, 
		esn_spectral_radius, esn_sparsity, esn_noise):
		
		pre = pre_process.Pre_process()

		self.pred_total = np.empty([self.future_total, self.future, 1])
		train_len = self.x_test.shape[1]

		esn = ESN(n_inputs = self.n_inputs,
			n_outputs = self.n_outputs,
			n_reservoir = esn_n_reservoir,
			spectral_radius = esn_spectral_radius,
			random_state = esn_random_state,
			sparsity = esn_sparsity,
			noise = esn_noise)

		print("Start training/predicting \n")    
		for i in range(self.x_test.shape[0]):
			pred_train = esn.fit(np.ones(train_len), self.x_test[i])
			prediction = esn.predict(np.ones(self.future))
			self.pred_total[i] = prediction

		print("Finished training/predicting \n")
		return self.pred_total



	def _optimise_sub_fun(self, args):
		"""
		Sub function for opt_hyperparamaters(.)

    	:param args: arguments to be passed into predict_ESN
    	:return: rmse error of predict_ESN, instead of a list of predicted values
		"""

		mean = np.mean(self.train)
		std = np.std(self.train)
		pre = pre_process.Pre_process()

		result = self.predict_ESN(23, args[0], args[1], args[2], args[3])
		inv_results = pre.inverse_tanh_estimator(mean, std, result)
		rmse_error = tools.Error.rmse(self.test, result)

		print("!!!!RMSE ERROR: {}".format(rmse_error))
		return rmse_error



	def opt_hyperparamaters(self, n_iter, init_points, n_reservoir_range, 
		spec_rad_range, sparsity_range, noise_range):
		"""
		Estimates the best paramaters for ESN
		
    	:param n_iter: number  of iterations of optimiser
    	:param init_points: number of inital points to randomly generate
    	:param :
    	:return: the full result
		"""

		res = gp_minimize(func = self._optimise_sub_fun, 
			dimensions =
			[Integer(n_reservoir_range[0], n_reservoir_range[1], name='esn_n_reservoir'), 
			Real(spec_rad_range[0], spec_rad_range[1], name='esn_spectral_radius'),
			Real(sparsity_range[0], sparsity_range[1], name='esn_sparsity'),
			Real(noise_range[0], noise_range[1], name='esn_noise')], 
			n_calls = n_iter, n_random_starts = init_points)
		print(res)

		plot_convergence(res)
		plt.show()
		return res



	def plot_results(self):
		train_len = len(self.train)

		plt.figure(figsize=(16,8))
		#plot original/integral
		plt.subplot(2, 1, 1)
		plt.plot(range(len(self.test)), self.test,'b',label="target system", alpha=0.3)
		plt.plot(range(self.future_total), self.pred_total,'k', label="free running ESN", alpha=0.8)

		#v-line for new prediction
		lo,hi = plt.ylim()
		for i in range(0, self.future_total, self.future):
			plt.plot([i, i],[lo+np.spacing(1),hi-np.spacing(1)],'y:', 
				linewidth=1)



		mean = np.mean(self.train)
		std = np.std(self.train)
		pre = pre_process.Pre_process()
		inv_results = pre.inverse_tanh_estimator(mean, std, self.pred_total)
		#inv_test = pre.inverse_tanh_estimator(mean, std, self.test)
		
		
		plt.subplot(2, 1, 2)
		plt.plot(range(len(self.test)), self.test,'b',label="target system", alpha=0.3)
		plt.plot(range(self.future_total), inv_results,'k', label="free running ESN", alpha=0.8)
		#v-line for new prediction
		lo,hi = plt.ylim()
		for i in range(0, self.future_total, self.future):
			plt.plot([i, i],[lo+np.spacing(1),hi-np.spacing(1)],'y:', 
				linewidth=1)
		


		#print(inv_test[:10])
		#print(inv_results[:10])

		print("rmse: " + str(tools.Error.rmse(self.test, self.pred_total)))
		print("rmse_2: " + str(tools.Error.rmse(self.test, inv_results)))
		print(f"NRMSD: {tools.Error.rmse(self.test, inv_results)/mean}")

		plt.show()







"""
Uses BayesianOptimization, but there is a problem within the classes, 
hence source code of ESN (the function) or the optimiser has to be changed.



from bayes_opt import BayesianOptimization

	def optimise_hyperparamaters(self, opt_n_iter, opt_init_points, n_reservoir_range, 
		spec_rad_range, sparsity_range, noise_range):

		opt = BayesianOptimization(f = self.opt_sub_fun, random_state = 1, 
			pbounds = {
			'esn_n_reservoir': n_reservoir_range,
			'esn_spectral_radius': spec_rad_range,
			'esn_sparsity': sparsity_range,
			'esn_noise': noise_range})

		opt.maximize(init_points = opt_init_points, n_iter = opt_n_iter)

		print(opt.max)
"""














"""
Does pyESN link the output back into the Reservoir?
"""